       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMI-CALCULATE.
       AUTHOR. CHENPOB.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  HEIGHT    PIC 999 VALUE ZERO .
       01  WEIGHT    PIC 999 VALUE ZERO.
       01  BMI PIC 99V9 VALUE ZERO .
           88 UNDER-WEIGHT-SHAPE VALUE 0 THRU 18.5.
           88 NORMAL-SHAPE       VALUE 18.6 THRU 24.9.
           88 OVER-WEIGHT-SHAPE  VALUE 25.0 THRU 29.9.
           88 OBSES-SHAPE        VALUE 30.0 THRU 34.9.
           88 EX-OBSES-SHAPE     VALUE 35.0 THRU HIGH-VALUE .
       01  SHAPE     PIC X(16).
       
       PROCEDURE DIVISION .
       BEGIN.
           DISPLAY "Input height (cm) - " WITH NO ADVANCING 
           ACCEPT HEIGHT 
           DISPLAY "Input weight (kg) - " WITH NO ADVANCING 
           ACCEPT WEIGHT 
           
           PERFORM BMI-CALCULATE
           PERFORM FIND-SHAPE
           GOBACK 
           .
       BMI-CALCULATE.
           COMPUTE BMI ROUNDED = WEIGHT / (HEIGHT / 100) ** 2
              ON SIZE ERROR DISPLAY "Error Bmi calculate"
           END-COMPUTE
           DISPLAY "Bmi score is " BMI
           EXIT
           .
       FIND-SHAPE.
           EVALUATE TRUE ALSO TRUE 
            WHEN UNDER-WEIGHT-SHAPE ALSO ANY  MOVE "Under Weight" 
               TO SHAPE 
            WHEN NORMAL-SHAPE ALSO ANY MOVE "Normal" TO SHAPE 
            WHEN OVER-WEIGHT-SHAPE ALSO ANY MOVE "Over Weight" TO SHAPE
            WHEN OBSES-SHAPE ALSO ANY MOVE "Obses" TO SHAPE  
            WHEN EX-OBSES-SHAPE ALSO ANY MOVE "Extremly Obses" TO SHAPE 
           END-EVALUATE  
           DISPLAY SHAPE 
           EXIT 
           .
